﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace catchACritter
{
    class Button
    {

        //----------------------
        //Data
        //----------------------
        Texture2D image = null;
        Vector2 position = Vector2.Zero;
        SoundEffect clickSFX;
        bool visible = true;

        //delegates
        public delegate void OnClick();
        public OnClick ourButtonCallback;
        //--------------------------
        //Behaviour
        //--------------------------
        public void LoadContent(ContentManager content)
        {
            
            image = content.Load<Texture2D>("graphics/button");
            
            clickSFX = content.Load<SoundEffect>("audio/buttonclick");
        }
        //---------------------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (visible == true)
            {
                spriteBatch.Draw(image, position, Color.White);
            }
        }
        //------------------------------------------------
        public void Input()
        {
            //get the current mouse state
            MouseState currentState = Mouse.GetState();

            //get the bounding box for critter
            Rectangle Bounds = new Rectangle(
                (int)position.X,
                (int)position.Y,
                image.Width,
                image.Height);

            //check if we have the left mouse button held down over the critter
            if (currentState.LeftButton == ButtonState.Pressed &&
                Bounds.Contains(currentState.X, currentState.Y) &&
                visible)
            {
                //we clicked the critter
                //play sound when critter is clicked
                clickSFX.Play();

                //despawn critter
                visible = false;

                if (ourButtonCallback != null)
                {
                    ourButtonCallback();
                }
                
            }
        }
        //-----------------------------------
    }
}
