﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace catchACritter
{
    class Critter
    {
        //----------------------
        //Type Definitions
        //----------------------
        public enum Species
            //a special type of integer with specific values, that have names
        {
            COW, //0
            NARWHAL,//1
            SLOTH,//2

            //------

            NUM//3
        }
        //----------------------
        //Data
        //----------------------
        Texture2D image = null;
        Vector2 position = Vector2.Zero;
        bool alive = false;
        Score scoreObject = null;
        int critterValue = 10;
        SoundEffect clickSFX;
        Species ourSpecies = Species.COW;

        //--------------------------
        //Behaviour
        //-----------------------------------------------------
        public Critter(Score newScore, Species newSpecies)
        {
            //called when object is created
            //no return type
            // helps decide how th object will be set u[
            //can have arguments
            //this one lets us have access to the games score
            scoreObject = newScore;
            ourSpecies = newSpecies;
        }
        //-----------------------------------------------------
        public void LoadContent(ContentManager content)
        {
            

            switch(ourSpecies)
            {
                case Species.COW:
                    image = content.Load<Texture2D>("graphics/cow");
                    critterValue = 5;
                    break;
                case Species.NARWHAL:
                    image = content.Load<Texture2D>("graphics/narwhal");
                    critterValue = 50;
                    break;
                case Species.SLOTH:
                    image = content.Load<Texture2D>("graphics/sloth");
                    critterValue = 10;
                    break;
                default:
                    break;      
            }

            clickSFX = content.Load<SoundEffect>("audio/buttonclick");
        }
        //---------------------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (alive)
            {
                spriteBatch.Draw(image, position, Color.White);
            }
        }
        //--------------------------------------------------
        public void Spawn(GameWindow window)
        {
            //set the critter to be alive
            alive = true;

            //Make bounds for random location of critter
            int positionYMin = 0;
            int positionXMin = 0;
            int positionYMax = window.ClientBounds.Height - image.Height;
            int positionXMax = window.ClientBounds.Width - image.Width;

            //generate random bounds for critter location
            Random rand = new Random();
            position.X = rand.Next(positionXMin, positionXMax);
            position.Y = rand.Next(positionYMin, positionYMax);
        }
        //--------------------------------------------------------------------
        public void Despawn()
        {
            //set criter to not alive to make it not draw
            alive = false;
        }
        //--------------------------------------------------
        public void Input()
        {
            //get the current mouse state
            MouseState currentState = Mouse.GetState();

            //get the bounding box for critter
            Rectangle critterBounds = new Rectangle(
                (int)position.X,
                (int)position.Y,
                image.Width,
                image.Height);

            //check if we have the left mouse button held down over the critter
            if (currentState.LeftButton == ButtonState.Pressed &&
                critterBounds.Contains(currentState.X,currentState.Y) &&
                alive)
            {
                //we clicked the critter
                //play sound when critter is clicked
                clickSFX.Play();

                //despawn critter
                Despawn();

                //add to score
                scoreObject.AddScore(critterValue);
            }
        }
    }
}
