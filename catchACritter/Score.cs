﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace catchACritter
{
    class Score
    {

        //----------------------
        //Data
        //----------------------
        int value = 0;
        Vector2 position = new Vector2(10,10);
        SpriteFont font = null;
        //-------------------------------------
        //Behaviour
        //-----------------------------------------
        public void LoadContent(ContentManager content)
        {
            //load the font from content
            font = content.Load<SpriteFont>("fonts/mainFont");
        }
        public void AddScore(int toAdd)
        {
            //adds the provided score to the current score;
            value += toAdd;
        }
        //---------------------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            //draw the score to the screen using the font variable
            spriteBatch.DrawString(font, "Score: " + value.ToString(), position, Color.White);
        }
        //---------------------------------------------------------------------------------------

    }
}
