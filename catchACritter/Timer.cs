﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace catchACritter
{
    class Timer
    {
        //----------------------
        //Data
        //----------------------
        float timeRemaining = 0;
        Vector2 position = new Vector2(10, 50);
        SpriteFont font = null;
        bool running = false;

        //delegates
        public delegate void TimeUp();
        public TimeUp ourTimerCallback;

        //-------------------------------------
        //Behaviour
        //----------------------------------------------------------
        public void LoadContent(ContentManager content)
        {
            //load the font from content
            font = content.Load<SpriteFont>("fonts/mainFont");
        }
        //--------------------------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            //draw the timer to the screen using the font variable
            int timeInt = (int)timeRemaining;
            spriteBatch.DrawString(font, "Time: " + timeInt.ToString(), position, Color.White);
        }
        //---------------------------------------------------------------
        public void Update(GameTime gameTime)
        {
            if (running)
            {
                timeRemaining -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                //if our time has run out
                if (timeRemaining <= 0)
                {
                    running = false;
                    timeRemaining = 0;

                    if (ourTimerCallback != null)
                    {
                        ourTimerCallback();
                    }
                }
            }
        }
        //---------------------------------------------------------------
        public void StartTimer()
        {
            running = true;
        }
        //--------------------------------------------
        public void SetTimer(float newTime)
        {
            timeRemaining = newTime;
        }
        //-------------------------------------------
    }
}
